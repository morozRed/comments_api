

/* global before, it, describe */

const User = require('../users/user.model');
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../app');

const { expect } = chai.expect;

chai.use(chaiHttp);

before((done) => {
  User.remove({});

  const testUser = new User({
    username: 'test',
    email: 'test@gmail.com',
    password: '123456',
  });

  testUser.save(() => {
    done();
  });
});

describe('Session', () => {
  describe('/POST create', () => {
    it('it should return access token and user', (done) => {
      const validSessionCredentials = {
        email: 'test@gmail.com',
        password: '123456',
      };
      chai.request(app)
        .post('/api/sessions')
        .send(validSessionCredentials)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.have.property('user');
          expect(res.body).to.have.property('token');
          done();
        });
    });
    it('it should not return token', (done) => {
      const invalidSessionCredentials = {
        email: 'test@gmail.com',
        password: '123',
      };
      chai.request(app)
        .post('/api/sessions')
        .send(invalidSessionCredentials)
        .end((err, res) => {
          expect(res).to.have.status(403);
          done();
        });
    });
  });
});
