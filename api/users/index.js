

const express = require('express');

const router = express.Router();
const controller = require('./user.controller');

router
  .post('/', controller.create)
  .get('/', controller.getAll);

module.exports = router;
