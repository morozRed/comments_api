

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const { Schema } = mongoose.Schema;

const User = new Schema({
  email: {
    type: String,
    required: 'User email can not be emty',
    unique: 'User with this email already exists',
  },
  password: {
    type: String,
    required: 'User password can not be emty',
  },
  username: {
    type: String,
    required: 'User name can not be empty',
    unique: 'User with this name already exists',
  },
});

User.pre('save', async function hashPassword(next) {
  const user = this;
  try {
    const hashedPassword = await bcrypt.hash(user.password, 10);
    user.password = hashedPassword;
    next();
  } catch (err) {
    next(err);
  }
});

mongoose.model('Users', User);
module.exports = mongoose.model('Users');
