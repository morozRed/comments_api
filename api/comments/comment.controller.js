

const asyncMiddleware = require('../../utils/asyncMiddleware');
const Comment = require('./comment.model');

module.exports = {
  create: asyncMiddleware(async (req, res) => {
    let parentComment;
    if (req.body.parentCommentId) {
      parentComment = await Comment.findOne({
        _id: req.body.parentCommentId,
      });
    }

    const newComment = await Comment({
      text: req.body.text || null,
      author: req.user.id,
      parent: parentComment || null,
    }).save();

    if (parentComment) {
      await parentComment.update({
        child: newComment.id,
      });
    }

    res.status(200).json({
      comment: newComment,
    });
  }),
  getAll: asyncMiddleware(async (req, res) => {
    const comments = await Comment.find({});
    res.status(200).json(comments);
  }),
  getMaxNestingLevel: asyncMiddleware(async (req, res) => {
    const nestingLevelResult = await Comment.aggregate([
      {
        $graphLookup: {
          from: 'comments',
          startWith: '$child',
          connectFromField: 'child',
          connectToField: '_id',
          as: 'childComments',
          depthField: 'nestingLevel',
        },
      },
      {
        $unwind: '$childComments',
      },
      {
        $group: {
          _id: 0,
          maxNestingLevel: { $max: '$childComments.nestingLevel' },
        },
      },
      {
        $project: {
          _id: 0,
          maxNestingLevel: 1,
        },
      },
    ]);
    // TODO: since depthFiled will be calculated from 0
    // probably not the best idea to do this...
    if (nestingLevelResult.length > 0) {
      nestingLevelResult[0].maxNestingLevel += 1;
      res.status(200).json(nestingLevelResult[0]);
    } else {
      res.status(200).json({ maxNestingLevel: 0 });
    }
  }),
};
