

const mongoose = require('mongoose');

const { Schema } = mongoose.Schema;

const Comment = new Schema({
  text: {
    type: String,
    required: 'Comment text can not be emty',
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'Users',
  },
  child: {
    type: Schema.Types.ObjectId,
    ref: 'Comments',
  },
  parent: {
    type: Schema.Types.ObjectId,
    ref: 'Comments',
  },
});

mongoose.model('Comments', Comment);

module.exports = mongoose.model('Comments');
